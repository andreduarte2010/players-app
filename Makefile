.PHONY: image publish run run-docker test change-version

# if you want to use your own registry, change "REGISTRY" value
#REGISTRY       = your.url.registry
REGISTRY_USER   = monolux
NAME            = players-app
VERSION			:= $(shell git describe)
IMAGE           = $(REGISTRY_USER)/$(NAME):$(VERSION)

image: guard-VERSION ## Build image
	docker build -t $(IMAGE) .

publish: guard-VERSION ## Publish image
	docker push $(IMAGE)

run: ## Run locally
	go run .

run-docker: guard-VERSION ## Run docker container
	docker run --rm -d --name $(NAME) -d -p 5000:5000 $(REGISTRY_USER)/$(NAME):$(VERSION)

test:
	go test -v -coverprofile=coverage.out ./...

change-version: guard-NEW_VERSION
	git tag -a $(NEW_VERSION) -m "Added version $(NEW_VERSION)"
	git push origin ${NEW_VERSION}

guard-%:
	@ if [ "${${*}}" = ""  ]; then \
		echo "Variable '$*' not set"; \
		exit 1; \
	fi
