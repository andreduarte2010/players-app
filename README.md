# Neoway DevOps TEST

This README is divide into three parts. The first part is about the Players App.
This part is practically the same as the one that was given in the start of the test. 
It shows how to build and run the Go api, and offers some examples on how to 
use the API. The second part explains how to deploy the players app CI/CD infrastructure. 
The third and last part, explains how the CI/CD workflow works, that is, how
the developers will create features and merge branches, and how theese changes
will be deployed. 

### INDEX

1. [Players App]( docs/app.md)
2. [Setup CI/CD Infra]( docs/ci-cd-infra.md)
3. [Pipeline Workflow]( docs/pipeline-wf.md)
 