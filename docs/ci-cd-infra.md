# CI/CD Infrastructure

For the CI/CD infrastructure of this project I have chosen 
technologies that are well integrated with one another and 
that are easy to use. I have preferred simplicity to customization, 
the reason behind this is that the test offers 
some constraints that we wouldn't see in a real world scenarios. 
Also, many projects don't need more customizability than the ones 
offered by the technologies and platforms that I've chosen. This 
will become clearer when I go into more detail  on the technologies 
down bellow. For now, here is a list of the technologies used:
  
  

- GCP GKE: Google's k8s platform is pretty simple to use and 
is well integrated with terraform.
- GitLab: It has a great CI/CD pipeline that is easy to use, 
and it's integration 
with GKE simplifies a lot of more mundane tasks.
- Terraform: Hashicorp's infrastructure automation application 
has become the de facto infrastructure automation tool. 


The steps down bellow will show you how to get the infrastructure 
up and running. It's divided into two parts, the first part will
show you how to deploy a k8s cluster in GCP, the second part will
integrate the k8s cluster created on the first part with GitLab.
  
    

1 - Create GKE cluster
--
The following steps expect that you have some specific tooling
preinstalled in your machine. If you don't, they are pretty easy
to install. The required tooling is listed down bellow, followed 
by links on how to install them:

1. Google Cloud SDK [gcloud](https://cloud.google.com/sdk/install)
2. Hashicorps's Terraform [terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)
3. Kube Control [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
  
Once you installed the tooling above, create a GCP project with
the name __players-app-staging__. And make sure your gcloud SDK
is connected to it with ``gcloud init``.

    
##### Bootstraping

 Creating a k8s is pretty easy with Terraform. It only requires
 a few configuration files, these files are available in the 
 `./infra/terraform` folder inside the repo. 
 If you navigate inside it, you'll notice two main configuration 
 files, one for
 the GKE cluster itself (I have chosen separatly managed as 
 recommended by GCP), and another file for the VPC configuration
 together with its subnet CIDR ranges. 
 
 >The file `terraform.tfvars` 
 contains the variables that terraform will use for it's configuration.
 You can look inside to see the names that will be used in the VPCs
 and cluster creation, along with the region in wich they will run.
 
 
 I've aggregated all the initial k8s cluster creation and configuration 
 commands in a shell file called **bootstrap.sh**. The commands are 
 basic terraform init and apply, together with a command that connects 
 your GKE with your local kubectl configs. It also creates a service 
 account in your cluster that the GitLab integration tool will use 
 to control the cluster.
 
 
 > Before you run the bootstrap shell script, change the variables in the 
 > **terraform.tfvars** file. There are two, the GCP project id, I have 
 > chosen **players-app-staging**, and the project region, in my case 
 > **us-east1**. Also make sure the Compute Engine API is [enabled](https://console.developers.google.com/apis/api/compute.googleapis.com/overview?project=players-app-staging)
 > and that Kubernetes Engine API is [also](https://console.cloud.google.com/apis/api/container.googleapis.com/overview?project=players-app-staging). 
 > ( both links assume your project id is `players-app-staging`).

 Finally, run the command in the **./infra/terraform** folder:
 
```sh
 ./bootstrap.sh
 ```
 
 If all goes well you are ready for the next step.
 
 2 - Integrating GitLab 
 ---
 GitLab offers great tooling for CI/CD pipelines. It also has great
 integration with k8s clusters. It can install Helm, Ingress and
 create a GitLab runner inside the cluster. It can also install
 Prometheus and generate info about the cluster by using it. GitLab
 offers a dashboard inside its own platform for this. 
 
> If your repo is not on GitLab, please create a repo now and
> your local repo to GitLab's. The following steps will require
> this.
 
 Access your project and go to __Operations> Kubernetes__ and click on 
 **Add Kubernetes cluster** button.
 
 ![alt text](images/gitlab-k8s-step1.png "Logo Title Text 1")
 
 On the tab that appears select __Add existing cluster__. You'll
 be greeted with a form containing various fields required to 
 connect to your existing GKE cluster. The fields should be
 filled in the following manner:
 
 Field Name | Value/Command
 --- | ---
 Kubernetes cluster name | **players-app-staging-gke**  
 Environment scope | **staging**  
 API URL | run ```kubectl cluster-info \| grep 'Kubernetes master' \| awk '/http/ {print $NF}'```  
 CA Certificate | run ```kubectl get secret $(kubectl get secrets \| awk 'NR==2{print $1}') -o jsonpath="{['data']['ca\.crt']}" \| base64 --decode```
 Service Token | run ```kubectl -n kube-system describe secret $(kubectl -n kube-system get secret \| grep gitlab-admin \| awk '{print $1}')```
 Project namespace prefix (optional, unique) | **gitlab**
 
> The commands above assume your kubectl config is setup with 
> you GKE cluster configurations.


Now that GitLab and GKE are connected, you'll be able to use one of gitlab's
more powerful features. For this, go to your project page then ``Operations>Kubernetes``,
select your cluster and click on the Applications tab. Here you'll see
all of the available apps that GitLab can install for you in the cluster.
All of them can be installed manually through Helm, but for the purpose of
this test, I'll stick to GitLab's automatic install. 

While still on the applications tab, install the following apps: Helm, Ingress
and GitLab runner. 

> Helm is an application installer for k8s cluster. It has many
> prepackaged applications with their predefined configurations.
> You could say helm is to the k8s cluster, what aptitude is to
> debian. Ingress is what k8s uses to communicate with the outside
> world. GitLab uses nginx's Ingress controller. [Here](https://cloud.google.com/community/tutorials/nginx-ingress-gke) is a great
> resource on how to install both Helm and Ingress(nginx)
> manually, without depending on GitLab to maintain it. There are
> many cases where this is preferable.


The infrastructure setup is now complete. Your GKE cluster will now
have all the necessary tools to start deploying your application
and making it available to the outside world. The next step will
show how your development workflow should go.



 
 