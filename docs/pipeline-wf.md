# Pipeline Workflow


#### General Overview of Developer Workflow
 The first part of starting a workflow is choosing a repository 
 layout. If we 
 follow CI/CD principals, we should have a structure similar to the
following: a protected master branch and a small and short-lived
features or hotfix branches that are merged through code review.
When branches are merged to the master branch, our GitLab CI/CD
will deploy to a staging environment. If the staging environment
is stable, then we can manually deploy to production.

###### CI/CD Stages
The infrastructure that we set up on the previous step, 
used three stages for our CI/CD: `test`, `publish` and `deploy`.
The first and second stages always run for any branch
that is pushed to the remote repo, but the last one only
runs when the master branch changes.

###### Versioning
You can use any versioning system that you find reasonable.
I've stuck to semantic versioning because of its widespread use
and intuitiveness. 

To change your repos version, all you need to do is add a tag to
a branch and push it up to the remote repo. For example, if
your new version is ``1.0.2`` then you'll need to run something
similar to:

```bash
git tag -a 1.0.2 -m "First major release" master
git push origin 1.0.2
```
 
 This will create a new version and this version will be tied 
 to the tag from the docker image that is built on the ``publish`` 
 stage. 
 
> The nice thing about this versioning system is that the version can 
>stay the same through different branches, but the tag will be different
>because a commit hash is added to the end of the tag.  


###### Testing The Staging Environment

The api works the same in the k8s staging environment as it would
locally. I've only taken the liberty to add ``/api/v1`` to the endpoint.

So to test the API, you'll need to get the Ingress external IP
address provided by GCP and make your requests as follows:

````bash
curl --location --request POST 'http://<GCP IP>/api/v1/players/<player-name>'
````