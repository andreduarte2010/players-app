#!/bin/bash

# Begins terraform and install necessary plugins
terraform init

# Run terraform apply to start GKE k8s-staging cluster
terraform apply -auto-approve

# Get credentials from GCP GKE cluster and apply them locally. This bootstrap assumes your region is "us-east1".
gcloud container clusters get-credentials players-app-staging-gke --region us-east1

# Create service account for GitLab integration. GitLab will use this service account to communicate with the cluster.
kubectl apply -f ../kubernetes/gitlab/gitlab-admin-service-account.yaml

